class CreateElementos < ActiveRecord::Migration
  def change
    create_table :elementos do |t|
      t.string :nombre
      t.string :descripcion

      t.timestamps
    end
  end
end
